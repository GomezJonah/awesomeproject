import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Button,
    Alert
} from 'react-native'

export default class Button extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Button
                   onPress={onPressLearnMore}
                    title="Learn More"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    }
})