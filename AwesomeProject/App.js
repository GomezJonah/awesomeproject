import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailsScreen'
import ActivityIndicator from './screens/ActivityIndicator'
import Button from './screens/Button'
import DrawerLayoutAndroid from './screens/DrawerLayoutAndroid'
import FlatList from './screens/FlatList'
import Image from './screens/Image'
import KeyboardAvoidingView from './screens/KeyboardAvoidingView'
import ListView from './screens/ListView'
import Modal from './screens/Modal'
import Picker from './screens/Picker'
import ProgressBarAndroid from './screens/ProgressBarAndroid'
import RefreshControl from './screens/RefreshControl'
import ScrollView from './screens/ScrollView'
import SectionList from './screens/SectionList'
import Slider from './screens/Slider'
import StatusBar from './screens/StatusBar'
import Switch from './screens/Switch'
import Text from './screens/Text'
import TextInput from './screens/TextInput'
import TouchableHighlight from './screens/TouchableHighlight'
import TouchableNativeFeedback from './screens/TouchableNativeFeedback'
import TouchableOpacity from './screens/TouchableOpacity'
import TouchableWithoutFeedback from './screens/TouchableWithoutFeedback'
import View from './screens/View'
import ViewPagerAndroid from './screens/ViewPagerAndroid'
import WebView from './screens/WebView';

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    ActivityIndicator: {
      screen: ActivityIndicator,
    },
    Button: {
      screen: Button,
    },
    DrawerLayout: {
      screen: DrawerLayoutAndroid,
    },
    FlatList: {
      screen: FlatList,
    },
    Image: {
      screen: Image,
    },
    KAV: {
      screen: KeyboardAvoidingView,
    },
    ListView: {
      screen: ListView,
    },
    Modal: {
      screen: Modal,
    },
    Picker: {
      screen: Picker,
    },
    ProgressBar: {
      screen: ProgressBarAndroid,
    },
    RefreshControl: {
      screen: RefreshControl,
    },
    ScrollView: {
      screen: ScrollView,
    },
    SectionList: {
      screen: SectionList,
    },
    StatusBar: {
      screen: StatusBar,
    },
    Switch: {
      screen: SwitchScreen,
    },
    Text: {
      screen: Text,
    },
    TextInput: {
      screen: TextInput,
    },
    TouchHigh: {
      screen: TouchableHighlight,
    },
    TouchFeedback: {
      screen: TouchableNativeFeedback,
    },
    TO: {
      screen: TouchableOpacity,
    },
    Touchablewo: {
      screen: TouchableWithoutFeedback,
    },
    View: {
      screen: View,
    },
    ViewPage: {
      screen: ViewPagerAndroid,
    },
    WebView: {
      screen: WebView,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends Component {
  render() {
    return <RootStack />
  }
}
